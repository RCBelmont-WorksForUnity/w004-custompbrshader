﻿//============================
//Created By RCBelmont on 2019年3月2日
//Description 自动生成一个Shader的Editor代码模板
//============================
using System.IO;
using UnityEditor;
using UnityEngine;

public class ShaderEditorScriptGen : EditorWindow
{
    [MenuItem("Tools/ShaderScriptGen")]
    public static void OpenWin()
    {
        EditorWindow win = GetWindow<ShaderEditorScriptGen>();
        win.Show();
    }

    private Shader _shader;

    private void OnGUI()
    {
        _shader = (Shader) EditorGUILayout.ObjectField("Shader", _shader, typeof(Shader), false);
        if (_shader)
        {
            if (GUILayout.Button("Test"))
            {
                string defualtPath = Application.dataPath;
                string shaderPath = AssetDatabase.GetAssetPath(_shader);
                shaderPath = Path.GetFileNameWithoutExtension(shaderPath);
                string path = EditorUtility.SaveFilePanel("SaveScript", defualtPath, shaderPath + "_Editor", "cs");
                CreateScriptFile(path, _shader);
                AssetDatabase.Refresh();
            }
        }
    }

    private void CreateScriptFile(string path, Shader shader)
    {
        if (!string.IsNullOrEmpty(path))
        {
            string fileName = Path.GetFileNameWithoutExtension(path);
            FileStream fs = new FileStream(path, FileMode.OpenOrCreate);
            StreamWriter sw = new StreamWriter(fs);
            sw.WriteLine("//Generate By Code");
            sw.WriteLine("using System;");
            sw.WriteLine("using System.Collections;");
            sw.WriteLine("using System.Collections.Generic;");
            sw.WriteLine("using UnityEngine;");
            sw.WriteLine("using UnityEngine.Rendering;");
            sw.WriteLine("using UnityEditor;");
            sw.WriteLine("internal class " + fileName + " : ShaderGUI");
            sw.WriteLine("{");
            sw.WriteLine("\toverride public void OnGUI(MaterialEditor materialEditor, MaterialProperty[] props)");
            sw.WriteLine("\t{");
            int count = ShaderUtil.GetPropertyCount(_shader);
            for (int i = 0; i < count; i++)
            {
                if (ShaderUtil.IsShaderPropertyHidden(_shader, i))
                {
                    continue;
                }
                string name = ShaderUtil.GetPropertyName(_shader, i);
                ShaderUtil.ShaderPropertyType type = ShaderUtil.GetPropertyType(_shader, i);
                string desc = ShaderUtil.GetPropertyDescription(_shader, i);
                sw.WriteLine("\t\t");
                sw.WriteLine("\t\t//Property" + i + ": " + name);
                sw.WriteLine("\t\tMaterialProperty " + name + " = FindProperty(\"" + name + "\", props);");
                switch (type)
                {
                    case ShaderUtil.ShaderPropertyType.Color:
                        sw.WriteLine("\t\tmaterialEditor.ColorProperty(" + name + ",\"" + desc + "\");");
                        break;
                    case ShaderUtil.ShaderPropertyType.Float:
                        sw.WriteLine("\t\tmaterialEditor.FloatProperty(" + name + ",\"" + desc + "\");");
                        break;
                    case ShaderUtil.ShaderPropertyType.Range:
                        sw.WriteLine("\t\tmaterialEditor.RangeProperty(" + name + ",\"" + desc + "\");");
                        break;
                    case ShaderUtil.ShaderPropertyType.Vector:
                        sw.WriteLine("\t\tmaterialEditor.VectorProperty(" + name + ",\"" + desc + "\");");
                        break;
                    case ShaderUtil.ShaderPropertyType.TexEnv:

                        sw.WriteLine("\t\tmaterialEditor.TexturePropertySingleLine(new GUIContent(\"" + desc +
                                     "\")," + name + ");");
                        sw.WriteLine("\t\tif((" + name + ".flags & MaterialProperty.PropFlags.NoScaleOffset) == 0)");
                        sw.WriteLine("\t\t{");
                        sw.WriteLine("\t\t\tmaterialEditor.TextureScaleOffsetProperty(" + name + ");");
                        sw.WriteLine("\t\t}");


                        break;
                }
            }

            sw.WriteLine("\t}");
            sw.WriteLine("}");


            sw.Close();
            fs.Close();
        }
    }
}